﻿using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace AzraAskepApiBackend.Code
{
    public class DbSql
    {
        #region Members

        /// <summary>
        /// The default connection string.
        /// </summary>
        public static readonly string DEFAULT_CONNECTION_STRING = AppConfig.Config["ConnectionStrings:AzraAskepDatabase"];

        /// <summary>
        /// The MySqlDB connection string.
        /// </summary>
        private string connString;

        /// <summary>
        /// The SQL Connection.
        /// </summary>
        private SqlConnection oConn;

        #endregion

        #region Constructors

        public DbSql(string connString)
        {
            this.connString = connString;
            oConn = new SqlConnection(connString);
        }

        public DbSql(string chooseDB, bool custom = true)
        {
            switch (chooseDB.ToLower())
            {
                case "Asskep":
                    oConn = new SqlConnection(DEFAULT_CONNECTION_STRING);
                    break;
                default:
                    oConn = new SqlConnection(DEFAULT_CONNECTION_STRING);
                    break;
            }
        }

        #endregion

        #region Methods

        public List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        object value = "";
                        if (!string.IsNullOrEmpty(dr[column.ColumnName].ToString()))
                        {
                            value = dr[column.ColumnName];
                        }
                        pro.SetValue(obj, value, null);
                    }
                    else
                        continue;
                }
            }
            return obj;
        }

        private bool OpenDB()
        {
            bool retVal = true;
            try
            {
                if (oConn.State != System.Data.ConnectionState.Open)
                    oConn.Open();
            }
            catch (Exception ex)
            {
                retVal = false;
                throw new Exception("Error openning database: " + ex.Message, ex);
            }
            return retVal;
        }

        private bool CloseDB()
        {
            bool retVal = true;
            try
            {
                if (oConn.State != System.Data.ConnectionState.Closed)
                    oConn.Close();
            }
            catch (Exception ex)
            {
                retVal = false;
                throw new Exception("Error closing database: " + ex.Message, ex);
            }

            return retVal;
        }

        public DataSet getDataSetFromSP(SqlCommand cmd)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = null;

            if (OpenDB())
                try
                {
                    cmd.Connection = oConn;
                    da = new SqlDataAdapter(cmd);

                    da.Fill(ds);
                    da.Dispose();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error executing database command: " + cmd.CommandText + "   " + ex.Message, ex);
                }

            da = null;
            CloseDB();
            return ds;
        }

        public static string GetJSONObjectString(DataTable Dt)
        {
            string[] StrDc = new string[Dt.Columns.Count];
            string HeadStr = string.Empty;

            for (int i = 0; i < Dt.Columns.Count; i++)
            {
                if ((Dt.Columns[i].Caption == "EvaluationQuestionTemplate") || (Dt.Columns[i].Caption == "EvaluationQuestionJson") || (Dt.Columns[i].Caption == "EvaluationAnswerJson"))
                {
                    StrDc[i] = Dt.Columns[i].Caption;
                    HeadStr += "\"" + StrDc[i] + "\" : " + StrDc[i] + i.ToString() + "¾" + ",";
                }
                else
                {
                    StrDc[i] = Dt.Columns[i].Caption;
                    HeadStr += "\"" + StrDc[i] + "\" : \"" + StrDc[i] + i.ToString() + "¾" + "\",";
                }
            }

            HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);

            StringBuilder Sb = new StringBuilder();

            // Console.WriteLine(Dt.Rows.Count);
            if (Dt.Rows.Count > 0)
            {
                // Sb.Append("{\"" + Dt.TableName + "\" : [");
                // Sb.Append("["); 
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    string TempStr = HeadStr;
                    Sb.Append("{");

                    for (int j = 0; j < Dt.Columns.Count; j++)
                    {
                        TempStr = TempStr.Replace(Dt.Columns[j] + j.ToString() + "¾", Dt.Rows[i][j].ToString());
                    }
                    Sb.Append(TempStr + "},");
                }

                Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));
                // Sb.Append("]}");
                // Sb.Append("]");
            }
            else
            {
                Sb.Append("{");
                Sb.Append("}");
            }


            return Sb.ToString();
        }

        public static string SerializeDataTable(DataTable Dt)
        {
            Dictionary<string, object> colValue = new Dictionary<string, object>();

            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow = new Dictionary<string, object>();

            string tablename = Dt.TableName;

            foreach (DataRow row in Dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in Dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }

            return JsonConvert.SerializeObject(parentRow);
        }

        public static string GetJSONArrayString(DataTable Dt)
        {
            string[] StrDc = new string[Dt.Columns.Count];
            string HeadStr = string.Empty;

            for (int i = 0; i < Dt.Columns.Count; i++)
            {
                if ((Dt.Columns[i].Caption == "EvaluationQuestionTemplate") || (Dt.Columns[i].Caption == "EvaluationQuestionJson") || (Dt.Columns[i].Caption == "EvaluationAnswerJson"))
                {
                    StrDc[i] = Dt.Columns[i].Caption;
                    HeadStr += "\"" + StrDc[i] + "\" : " + StrDc[i] + i.ToString() + "¾" + ",";
                }
                else
                {
                    StrDc[i] = Dt.Columns[i].Caption;
                    HeadStr += "\"" + StrDc[i] + "\" : \"" + StrDc[i] + i.ToString() + "¾" + "\",";
                }
            }

            HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);

            StringBuilder Sb = new StringBuilder();

            // Console.WriteLine(Dt.Rows.Count);
            if (Dt.Rows.Count > 0)
            {
                // Sb.Append("{\"" + Dt.TableName + "\" : [");
                Sb.Append("[");
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    string TempStr = HeadStr;
                    Sb.Append("{");

                    for (int j = 0; j < Dt.Columns.Count; j++)
                    {
                        TempStr = TempStr.Replace(Dt.Columns[j] + j.ToString() + "¾", Dt.Rows[i][j].ToString());
                    }
                    Sb.Append(TempStr + "},");
                }

                Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));
                // Sb.Append("]}");
                Sb.Append("]");
            }
            else
            {
                Sb.Append("[");
                Sb.Append("]");
            }


            return Sb.ToString();
        }


        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion
    }
    public static class AppConfig
    {
        public static IConfigurationRoot Config = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();
    }
}
