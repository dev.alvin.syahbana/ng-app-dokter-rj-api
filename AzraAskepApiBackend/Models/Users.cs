﻿using System;
using System.Collections.Generic;

namespace AzraAskepApiBackend.Models
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public string Level { get; set; }
        public string SubLevel { get; set; }
        public string Deskripsi { get; set; }
    }
}
