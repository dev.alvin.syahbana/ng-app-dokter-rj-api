using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;


namespace AzraAskepApiBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/AppDokterRawatJalan")]
    public class AppDokterRawatJalanController : Controller
    {
        private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public AppDokterRawatJalanController(ASKEPContext context)
        {
            _context = context;
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // start api baru utk app dokter
        // api/AppDokterRawatJalan/GetPoli
        [HttpGet("GetPoli")]
        public IActionResult GetPoli()
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPoli";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/GetSatuan
        [HttpGet("GetSatuan")]
        public IActionResult GetSatuan()
        {
            string query = "";
            string res = "";

            try
            {
                query = "SELECT * FROM MasterSatuan";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/DokterPraktek
        [HttpGet("DokterPraktek/{kd_poli}")]
        public IActionResult DokterPraktek(string kd_poli)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_DokterPraktek @kd_poli='" + kd_poli + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/JamPrakterByKdDok
        [HttpGet("JamPrakterByKdDok/{kd_dok}")]
        public IActionResult JamPrakterByKdDok(string kd_dok)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_JamPraktekDokter @Kd_Dokter='" + kd_dok + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/CountListPasienRJDokter
        [HttpPost("CountListPasienRJDokter")]
        public IActionResult CountListPasienRJDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_CountListPasienRJDokter @tgl_masuk = '" + data.tglMasuk + "', @kd_lyn = '" + data.kdLyn + "', " +
                    "@kd_dokter = '" + data.kdDokter + "', @jam_praktek = '" + data.jamPraktek + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/ListPasienRJDokter
        [HttpPost("ListPasienRJDokter")]
        public IActionResult ListPasienRJDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_ListPasienRJDokter @tgl_masuk = '" + data.tglMasuk + "', @kd_lyn = '" + data.kdLyn + "', " +
                    "@kd_dokter = '" + data.kdDokter + "', @jam_praktek = '" + data.jamPraktek + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/GetPasienByRM
        [HttpGet("GetPasienByRM/{rj_noreg}/{kd_rm}")]
        public IActionResult GetPasienByRM(string rj_noreg, string kd_rm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetPasienDetail @Kd_RM = '" + kd_rm + "',  @RJ_NoReg = '" + rj_noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/Broadcast
        [HttpPost("Broadcast")]
        public IActionResult Broadcast([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_broadcast @No_Karcis = '" + data.no_karcis + "', @Tgl_Masuk = '" + data.tanggal_masuk + "', " +
                    "@Kd_Dokter = '" + data.kode_dokter + "', @Jam_Praktek = '" + data.jam_praktek + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/CekTicketStstus
        [HttpGet("CekTicketStstus/{no_reg}")]
        public IActionResult GetPasienByRM(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_cekTicketStsByNoReg @No_Reg = '" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/SendBroadcast
        [HttpPost("SendBroadcast")]
        public IActionResult SendBroadcast([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_sendBroadCast @No_Reg = '" + data.no_reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("GetPenunMedisByNoreg")]
        public IActionResult GetPenunMedisByNoreg([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPelPenunjangMedis @Kd_Dokter = '" + data.Kd_Dokter + "', @No_Reg = '" + data.No_Reg + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = DbSql.GetJSONArrayString(ds.Tables[0]);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }
            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/InstPenunMedisH
        [HttpPost("InstPenunMedisH")]
        public IActionResult InstPenunMedisH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";
            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..NewInsTrxLaboratorium11 @No_Trx = '" + data[0].No_Trx + "', @Kd_Lyn = '" + data[0].Kd_Lyn + "', @No_Reg = '" + data[0].No_Reg + "', @No_Urut = '" + data[0].No_Urut + "', @Tgl_Periksa = '" + data[0].Tgl_Periksa + "', @Kd_Dokter_Kirim = '" + data[0].Kd_Dokter_Kirim + "', @Dokter_Kirim = '" + data[0].Dokter_Kirim + "', @Kd_DrLab = '" + data[0].Kd_DrLab + "', @Kd_PenataLab = '" + data[0].Kd_PenataLab + "',  @Jumlah = '" + data[0].TotalH + "', @Discount = '" + data[0].Discount + "', @Karyawan = '" + data[0].StatKaryawan + "', @FOC = '" + data[0].FOC + "', @UserID = '" + data[0].UserID + "', @KD_Kelas = '" + data[0].KD_Kelas + "', @Kunjungan = '" + data[0].Kunjungan + "', @Total = '" + data[0].TotalH + "'";

                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\""+ sb.ToString() +"\"}";
            }
            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/UpdtPenunMedisH
        [HttpPost("UpdtPenunMedisH")]
        public IActionResult UpdtPenunMedisH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";
            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..UpdtTrxLaboratorium11 @No_Trx = '" + data[0].No_Trx + "', @Kd_Lyn = '" + data[0].Kd_Lyn + "', @No_Reg = '" + data[0].No_Reg + "', @No_Urut = '" + data[0].No_Urut + "', @Tgl_Periksa = '" + data[0].Tgl_Periksa + "', @Kd_Dokter_Kirim = '" + data[0].Kd_Dokter_Kirim + "', @Dokter_Kirim = '" + data[0].Dokter_Kirim + "', @Kd_DrLab = '" + data[0].Kd_DrLab + "', @Kd_PenataLab = '" + data[0].Kd_PenataLab + "',  @Jumlah = '" + data[0].TotalH + "', @Discount = '" + data[0].Discount + "', @Karyawan = '" + data[0].StatKaryawan + "', @FOC = '" + data[0].FOC + "', @UserID = '" + data[0].UserID + "', @KD_Kelas = '" + data[0].KD_Kelas + "', @Kunjungan = '" + data[0].Kunjungan + "', @Total = '" + data[0].TotalH + "'";

                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }
            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/InstPenunMedisD
        [HttpPost("InstPenunMedisD")]
        public IActionResult InstPenunMedisD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE IMAN..NewSaveTrxLaboratorium_DT2 @No_Trx = '" + data[i].No_Trx + "', @Kd_Lyn = '" + data[i].Kd_Lyn + "', @Kd_Tindakan = '" + data[i].Kd_Tindakan + "', @CITO = '" + data[i].StatCITO + "', @Harga = '" + data[i].Harga + "', @Discount = '" + data[i].Discount + "', @UserID = '" + data[i].UserID + "', @jmlTrx = '" + data[i].JmlTrx + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                    // res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/UpdtPenunMedisD
        [HttpPost("UpdtPenunMedisD")]
        public IActionResult UpdtPenunMedisD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE IMAN..UpdtTrxLaboratorium_DT2 @No_Trx = '" + data[i].No_Trx + "', @Kd_Lyn = '" + data[i].Kd_Lyn + "', @Kd_Tindakan = '" + data[i].Kd_Tindakan + "', @CITO = '" + data[i].StatCITO + "', @Harga = '" + data[i].Harga + "', @Discount = '" + data[i].Discount + "', @UserID = '" + data[i].UserID + "', @jmlTrx = '" + data[i].JmlTrx + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                    // res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/DelPenunMedis
        [HttpPost("DelPenunMedis")]
        public IActionResult DelPenunMedis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..DelPelayananPenunjangMedis @No_Trx = '" + data.No_Trx + "', @No_Reg = '" + data.No_Reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/DelTindakanPenunMedis
        [HttpPost("DelTindakanPenunMedis")]
        public IActionResult DelTindakanPenunMedis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..DelTindakanPelayananPenunjangMedis @No_Trx = '" + data.No_Trx + "', @Kd_Tindakan = '" + data.Kd_Tindakan + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/GetEditTindMedis
        [HttpPost("GetEditTindMedis")]
        public IActionResult GetEditTindMedis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..sp_GetTindakanPenunjangMedis @No_Trx = '" + data.No_Trx + "', @Kd_Dokter = '" + data.Kd_Dokter + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/GetPaketPMByDok
        [HttpGet("GetPaketPMByDok/{kddok}/{kdlyn}")]
        public IActionResult GetPaketPMByDok(string kddok, string kdlyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPaketPM @Kd_Dokter = '" + kddok + "', @Kd_Lyn = '" + kdlyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/GetPemeriksaanByPaketPM
        [HttpGet("GetPemeriksaanByPaketPM/{kddok}/{kdtemp}/{kdplyn}")]
        public IActionResult GetPemeriksaanByPaketPM(string kddok, string kdtemp, string kdplyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPaketPMDetail @Kd_Dokter = '" + kddok + "', @Kd_Template = '" + kdtemp + "', @Kd_Lyn = '" + kdplyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/InstPaketPM
        [HttpPost("InstPaketPM")]
        public IActionResult InstPaketPM([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Insert_PaketPenunjangMedis @kd_dokter = '" + data.kd_dokter + "', @nm_template = '" + data.nm_template + "', @kd_poli = '" + data.kd_poli + "', @kd_lyn = '" + data.kd_lyn + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/InstPaketPMDT
        [HttpPost("InstPaketPMDT")]
        public IActionResult InstPaketPMDT([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_Insert_PaketPenunjangMedis_dt @kd_dokter = '" + data.kd_dokter + "', @kd_tindakan = '" + data.data[i].Kd_Tindakan + "', " +
                    "@jml_so = '" + data.data[i].JmlTrx + "', @kd_poli = '" + data.kd_poli + "', @kd_lyn = '" + data.data[i].Kd_Lyn + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                    // res = DbSql.GetJSONObjectString(ds.Tables[0]);

                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/InstPaketPMDT
        [HttpPost("DelPaketPM")]
        public IActionResult DelPaketPM([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Delete_PaketPenunjangMedis @kd_dokter = '" + data.kd_dokter + "', @kd_template = '" + data.kd_template + "', @kd_lyn = '" + data.kd_lyn + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/ListJasaDokter
        [HttpPost("ListJasaDokter")]
        public IActionResult ListJasaDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GetJasaDokter @kd_dokter = '" + data.kd_dokter + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRawatJalan/InstJasaDokter
        [HttpPost("InstJasaDokter")]
        public IActionResult InstJasaDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InstJasaDokter @noreg = '" + data.noreg + "', @kdlyn = '" + data.kdlyn + "', @kddokter = '" + data.kddokter + "', @qty = '" + data.qty + "', @cito = '" + data.cito + "',  @hrg = '" + data.hrg + "', @disc = '" + data.disc + "', @total = '" + data.total + "', @hrgnormal = '" + data.hrgnormal + "', @hrgcito = '" + data.hrgcito + "', @hrgkary = '" + data.hrgkary + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = DbSql.GetJSONArrayString(ds.Tables[0]);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRawatJalan/CheckJasaDokter
        [HttpPost("CheckJasaDokter")]
        public IActionResult CheckJasaDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_CheckJasaDokter @noreg = '" + data.noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

    }
}