using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/AppDokterRJDiag")]
    public class AppDokterRJDiagController : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public AppDokterRJDiagController(ASKEPContext context)
        {
            _context = context;
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // note: api get diagnosa untuk pasien
        // api/AppDokterRJDiag/GetDiagnosaPasien
        [HttpGet("GetDiagnosaPasien/{deskterjemah}")]
        public IActionResult GetDiagnosaPasien(string deskterjemah)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetDiagnosaPasein @DESCRIPTION = '"+ deskterjemah + "', " + 
                "@TERJEMAHAN = '"+ deskterjemah + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = UsersController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJDiag/GetDiagnosaPasienByNoReg
        [HttpGet("GetDiagnosaPasienByNoReg/{NoReg}")]
        public IActionResult GetDiagnosaPasienByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_DiagnosaPasienDetail @RJ_NoReg = '" + NoReg + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/GetSkrinningPasien
        [HttpGet("GetSkrinningPasien/{noreg}")]
        public IActionResult GetSkrinningPasien(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetSkrinningPasienRJ @NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/GetAllDiagnosaPasien
        [HttpGet("GetAllDiagnosaPasien/{kdrm}")]
        public IActionResult GetAllDiagnosaPasien(string kdrm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetAllRiwayatDiagnosaPasien @Kd_RM='" + kdrm + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJDiag/GetDetailDiagnosaPasien
        [HttpGet("GetDetailDiagnosaPasien/{kdrm}/{noreg}")]
        public IActionResult GetDetailDiagnosaPasien(string kdrm, string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetDetailRiwayatDiagnosaPasien @Kd_RM='" + kdrm + "', @NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Start Riwayat Resep Non Racik
        // api/AppDokterRJDiag/GetAllRiwayatResepNRacik
        [HttpGet("GetAllRiwayatResepNRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepNRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetAllResepObatNRacik @NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Start Riwayat Resep Racik
        // api/AppDokterRJDiag/GetAllRiwayatResepRacik
        [HttpGet("GetAllRiwayatResepRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetAllResepObatRacik @NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJDiag/UpdateSkrinningPasien
        [HttpPut("UpdateSkrinningPasien")]
        public IActionResult UpdateSkrinningPasien([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateSkrinningPasien @KeluhanUtama = '" + data.KeluhanUtama + "', " + 
                "@NoReg = '" + data.NoReg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Insert RJ Diagnosa
        // api/AppDokterRJDiag/InsertRJDiagnosa
        [HttpPost("InsertRJDiagnosa")]
        public IActionResult InsertRJDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertRJ_Diagnosa @RJ_NoReg = '" + data.RJ_NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Dokter = '" + data.Kd_Dokter + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // tidak dipakai
        // api/AppDokterRJDiag/UpdateRJDiagnosaxxx
        [HttpPut("UpdateRJDiagnosaxxx")]
        public IActionResult UpdateRJDiagnosaxxx([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                string count = "SELECT COUNT(*) C FROM sirs..icdx WHERE [DESCRIPTION] + ' | ' + [TERJEMAHAN] = '" + data.Ket_Tindakan + "'";
                sb.AppendFormat(count);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string cek = DbSql.GetJSONObjectString(ds.Tables[0]);
                if (JObject.Parse(cek)["C"].ToString() == "0")
                {
                    query = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '', @Ket_ICD = '" + data.Ket_Tindakan + "', " + 
                    "@UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                }
                else
                {
                    query = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', " + 
                    "@UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                }

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/UpdateRJDiagnosa
        [HttpPut("UpdateRJDiagnosa")]
        public IActionResult UpdateRJDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/InsertDiagnosaDetail
        [HttpPost("InsertDiagnosaDetail")]
        public IActionResult InsertDiagnosaDetail([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_InsertDiagnosaDetail @RJ_NoReg = '" + data.RJ_NoReg + "', @Kd_ICD='" + data.data[i].ICD_CODE + "', " +
                    "@English_ICD='" + data.data[i].DESCRIPTION + "', @UserID = '" + data.UserID + "'";
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/UpdatePemeriksaan
        [HttpPut("UpdatePemeriksaanFisik")]
        public IActionResult UpdatePemeriksaanFisik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdatePemeriksaanFisik @PemeriksaanFisik = '" + data.PemeriksaanFisik + "', " + 
                "@Planning = '" + data.Planning + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Alergi
        // api/AppDokterRJDiag/UpdateALergidbPasienRS
        [HttpPut("UpdateALergidbPasienRS/{kd_rm}")]
        public IActionResult UpdateALergidbPasienRS([FromBody] dynamic data, string kd_rm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateAlergidbPasienRS @Alergi = '" + data.alergi + "',  @Kd_RM = '" + kd_rm + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/UpdateALergiApSOHeader
        [HttpPut("UpdateALergiApSOHeader/{no_reg}")]
        public IActionResult UpdateALergiApSOHeader([FromBody] dynamic data, string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateAlergiApSOHeader @Alergi = '" + data.alergi + "',  @NoReg = '" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/CekDiagnosaByRmNoReg
        [HttpGet("CekDiagnosaByRmNoReg/{kdrm}/{noreg}")]
        public IActionResult CekDiagnosaByRmNoReg(string kdrm, string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekDiagnosaByRmReg @Kd_RM='" + kdrm + "', @RJ_NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJDiag/GetDiagnosaDetailByNoReg
        [HttpGet("GetDiagnosaDetailByNoReg/{NoReg}")]
        public IActionResult GetDiagnosaDetailByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_DiagnosaDetail @NoReg = '" + NoReg + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJDiag/GetTTVByNoReg
        [HttpGet("GetTTVByNoReg/{NoReg}")]
        public IActionResult GetTTVByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_DiagnosaTTV @NoReg = '" + NoReg + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
    }
}