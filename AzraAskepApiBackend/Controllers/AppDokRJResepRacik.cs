using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/AppDokterRJResepRacik")]
    public class AppDokterRJResepRacik : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public AppDokterRJResepRacik(ASKEPContext context)
        {
            _context = context;
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // api/AppDokterRJResepRacik/GetNamaRacik
        // api untuk menmapilkan racikan baru yg blm punya no so
        [HttpGet("GetNamaRacik/{no_so}")]
        public IActionResult GetNamaRacik(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetDataRacik @no_so='" + no_so.Replace("@","/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api untuk menampilkan data racik obat ketika update resep
        // api/AppDokterRJResepRacik/GetResepObatRacik
        [HttpGet("GetResepObatRacik/{no_reg}")]
        public IActionResult GetResepObatRacik(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetResepObatRacik @No_Reg='" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJResepRacik/GetResepObatRacikDetail
        [HttpGet("GetResepObatRacikDetail/{no_so}/{kd_racik}")]
        public IActionResult GetResepObatRacikDetail(string no_so, string kd_racik)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetResepObatRacikDetail @no_so='" + no_so.Replace("@","/") + "', @kd_racik='" + kd_racik +"'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJResepRacik/GenerateRacikHeader
        [HttpPost("GenerateRacikHeader")]
        public IActionResult GenerateRacikHeader([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GenerateRacikHeader @no_so = '" + data.no_so + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepRacik/CekNoSOApSORacikH
        [HttpGet("CekNoSOApSORacikH/{no_so}")]
        public IActionResult CekNoSOApSORacikH(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekNoSOApSORacikH @No_SO = '" + no_so.Replace("@","/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: api get paket obat racik berdasarkan kode dokter
        // api/AppDokterRJResepRacik/GetPaketObatRacikByDok
        [HttpGet("GetPaketObatRacikByDok/{kddok}")]
        public IActionResult GetPaketObatRacikByDok(string kddok)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPaketObatRacik @Kd_Dokter = '"+ kddok +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // note: api menampilkan list obat berdasrkan paket obat yg dipilih pada paket obat non racik
        // api/AppDokterRJResepRacik/GetObatByPaketObatRacik
        [HttpGet("GetObatByPaketObatRacik/{kddok}/{kdpkt}")]
        public IActionResult GetObatByPaketObatRacik(string kddok, string kdpkt)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPakeObatRacikDetail @Kd_Dokter = '"+ kddok +"', @Kd_Template = '"+ kdpkt +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: membuat paket obat baru pada paket obat racik
        // api/AppDokterRJResepRacik/PostApSOTemplate_Resep_Racik
        [HttpPost("PostApSOTemplate_Resep_Racik")]
        public IActionResult PostApSOTemplate_Resep_Racik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Insert_ApSOTemplate_Resep_Racik @kd_dokter = '" + data.kd_dokter + "', " + 
                "@nm_template = '" + data.nm_template + "', @kd_poli = '" + data.kd_poli + "', " + 
                "@satuan = '" + data.satuan + "', @aturan_pakai = '" + data.aturan_pakai + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: menambahkan obat pada paket obat berdasarkan kd_dokter pada paket obat racik
        // api/AppDokterRJResepRacik/PostApSOTemplate_Resep_Racik_dt
        [HttpPost("PostApSOTemplate_Resep_Racik_dt")]
        public IActionResult PostApSOTemplate_Resep_Racik_dt([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_Insert_ApSOTemplate_Resep_Racik_dt @kd_dokter = '" + data.kd_dokter + "', " + 
                    "@kd_template = '" + data.kd_template + "', @kd_poli = '" + data.kd_poli + "', " + 
                    "@jumlah = '" + data.data[i].Jml_SO + "', @satuan = '" + data.data[i].Sat_Brg + "', " + 
                    "@kd_barang = '" + data.data[i].Kd_Brg + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // post ke tabel ApSoRacikH
        // api/AppDokterRJResepRacik/PostApSoRacikH
        [HttpPost("PostApSoRacikH")]
        public IActionResult PostApSoRacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertApSoRacikH @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                    "@no_urut = '" + data.no_urut + "', @nama_racikan = '" + data.nama_racikan + "', @jml_ns = '" + data.jml_ns + "', @satuan_racik = '" + data.satuan_racik + "', @dosis_racik = '" + data.dosis_racik + "', @info_obat_racik = '" + data.info_obat_racik + "', @catatan_racik = '" + data.catatan_racik + "', @ket_racik = '" + data.ket_racik + "', @jml_rmw = '" + data.jml_rmw + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepRacik/PostApSORacikD
        [HttpPost("PostApSORacikD")]
        public IActionResult PostApSORacikD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.dataRacik.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_InsertApSORacikD @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                        "@no_urut = '" + data.no_urut + "', @kd_brg='" + data.dataRacik[i].Kd_Brg + "', @jumlah='" + data.dataRacik[i].Jml_SO + "', @sat_brg='" + data.dataRacik[i].Sat_SO + "'";
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepRacik/DeleteApSORacikH
        [HttpPost("DeleteApSORacikH")]
        public IActionResult DeleteApSORacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteApSORacikH @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepRacik/DeleteApSORacikD
        [HttpPost("DeleteApSORacikD")]
        public IActionResult DeleteApSORacikD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteApSORacikD @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                    "@kd_brg = '" + data.kd_brg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
    }
}