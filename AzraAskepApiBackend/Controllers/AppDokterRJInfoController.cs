using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/AppDokterRJInfo")]
    public class AppDokterRJInfoController : Controller
    {
        private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public AppDokterRJInfoController(ASKEPContext context)
        {
            _context = context;
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // api/AppDokterRJInfo/StatusAwalPraktekDokter
        [HttpPost("StatusAwalPraktekDokter")]
        public IActionResult StatusAwalPraktekDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_StatusAwalPrakterDokter @Kd_Poli = '" + data.Kd_Poli + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Status Praktek Mulai
        // api/AppDokterRJInfo/UpdateStatusPraktekMulai
        [HttpPut("UpdateStatusPraktekMulai")]
        public IActionResult UpdateStatusPraktekMulai([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateStatusPraktekMulai @Kd_Lyn = '" + data.Kd_Lyn + "', " +
                "@Kd_Dokter = '" + data.Kd_Dokter + "', @Jam_Praktek = '" + data.Jam_Praktek + "', " +
                "@Kd_Poli = '" + data.Kd_Poli + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Status Praktek Selesai
        // api/AppDokterRJInfo/UpdateStatusPraktekMulai
        [HttpPut("UpdateStatusPraktekSelesai")]
        public IActionResult UpdateStatusPraktekSelesai([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateStatusPraktekSelesai @Kd_Poli = '" + data.Kd_Poli + "', " +
                "@Tgl_Masuk = '" + data.Tgl_Masuk + "', @Kd_Lyn = '" + data.Kd_Lyn + "', " +
                "@Kd_Dokter = '" + data.Kd_Dokter + "',  @Jam_Praktek = '" + data.Jam_Praktek + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJInfo/AntrianTiket
        [HttpPost("AntrianTiket")]
        public IActionResult AntrianTiket([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRS.dbo.InsQCalling @Ticket_ID = '" + data.ticket_id + "', " +
                "@Counter_No = '" + data.counter_no + "', @Ticket_Status = '" + data.ticket_status + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // api/AppDokterRJInfo/BroadcastWA
        [HttpPost("BroadcastWA")]
        public IActionResult BroadcastWA([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_insQcallingwa @Kd_Lyn = '" + data.Kd_Lyn + "', " +
                "@Kd_Dokter = '" + data.Kd_Dokter + "', @Jam_Praktek = '" + data.Jam_Praktek + "', @rj_noreg = '" + data.rj_noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Update Status Panggil 
        // api/AppDokterRJInfo/UpdStsPanggil
        [HttpPost("UpdStsPanggil")]
        public IActionResult UpdStsPanggil([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRSINFO.dbo.updstatusPanggil @Ticket_ID = '" + data.ticket_id + "', " +
                "@Ticket_Posisi = '" + data.ticket_posisi + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJInfo/CheckJadwal
        [HttpPost("CheckJadwal")]
        public IActionResult CheckJadwal([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRS..sp_GetJadwalDokter @kdlyn = '" + data.kdlyn + "', @kddokter = '" + data.kddokter + "', @jam = '" + data.jam + "', @hari = '" + data.hari + "', @kdpoli = '" + data.kdpoli + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJInfo/UpdateStatPraktek
        [HttpPost("UpdateStatPraktek")]
        public IActionResult UpdateStatPraktek([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRS..sp_PutDisplayJadwalPraktek @stat = '" + data.stat + "', @kdpoli = '" + data.kdpoli + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJInfo/UpdateInfoPraktek
        [HttpPost("UpdateInfoPraktek")]
        public IActionResult UpdateInfoPraktek([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRSINFO..sp_PutInfoDisplay @kdpoli = '" + data.kdpoli + "', @info = '" + data.info + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("InsertLogSystem")]
        public IActionResult InsertLogSystem([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRSINFO..sp_InstLogAppDokter @NoReg = '" + data.NoReg + "', @Kd_Dokter = '" + data.Kd_Dokter + "', @Aksi = '" + data.Aksi + "', @Jenis = '" + data.Jenis + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
            }

            return Ok(JObject.Parse(res));
        }
    }
}