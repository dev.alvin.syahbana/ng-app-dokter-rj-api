﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{

    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public UsersController(ASKEPContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<Users> GetUsers()
        {
            return _context.Users;
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsers([FromRoute] int id, [FromBody] Users users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != users.Id)
            {
                return BadRequest();
            }

            _context.Entry(users).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUsers([FromBody] Users users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Users.Add(users);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UsersExists(users.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUsers", new { id = users.Id }, users);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (users == null)
            {
                return NotFound();
            }

            _context.Users.Remove(users);
            await _context.SaveChangesAsync();

            return Ok(users);
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // start api baru utk app dokter
        // api/Users/GetPoli
        [HttpGet("GetPoli")]
        public IActionResult GetPoli()
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPoli";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // api/Users/DokterPraktek
        [HttpGet("DokterPraktek/{kd_poli}")]
        public IActionResult DokterPraktek(string kd_poli)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_DokterPraktek @kd_poli='" + kd_poli + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/JamPrakterByKdDok
        [HttpGet("JamPrakterByKdDok/{kd_dok}")]
        public IActionResult JamPrakterByKdDok(string kd_dok)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_JamPraktekDokter @Kd_Dokter='" + kd_dok + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/ListPasienRJDokter
        [HttpPost("ListPasienRJDokter")]
        public IActionResult ListPasienRJDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_ListPasienRJDokter @tgl_masuk = '"+ data.tglMasuk + "', @kd_lyn = '" + data.kdLyn+ "', " +
                    "@kd_dokter = '" + data.kdDokter + "', @jam_praktek = '" + data.jamPraktek + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/GetPasienByRM
        [HttpGet("GetPasienByRM/{rj_noreg}/{kd_rm}")]
        public IActionResult GetPasienByRM(string rj_noreg, string kd_rm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetPasienDetail @Kd_RM = '" + kd_rm + "',  @RJ_NoReg = '" + rj_noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/ObatResep
        [HttpGet("ObatResep/{kddok}")]
        public IActionResult ObatResep(string kddok)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_DataObat @kddok='" + kddok + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = UsersController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Paket obat non racik
        // note: api get paket obat non racik berdasarkan kode dokter
        // api/Users/GetPaketObatByDok
        [HttpGet("GetPaketObatByDok/{kddok}")]
        public IActionResult GetPaketObatByDok(string kddok)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPaketObatNonRacik @Kd_Dokter = '"+ kddok +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // note: api menampilkan list obat berdasrkan paket obat yg dipilih pada paket obat non racik
        // api/Users/GetObatByPaketObat
        [HttpGet("GetObatByPaketObat/{kddok}/{kdpkt}")]
        public IActionResult GetObatByPaketObat(string kddok, string kdpkt)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPaketObatNonRacikDetail @Kd_Dokter = '"+ kddok +"',  @Kd_Template = '"+ kdpkt +"'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: membuat paket obat baru pada paket obat non racik
        // api/Users/PostApSOTemplate_Resep
        [HttpPost("PostApSOTemplate_Resep")]
        public IActionResult PostApSOTemplate_Resep([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Insert_ApSOTemplate_Resep @kd_dokter = '" + data.kd_dokter + "', @nm_template = '" + data.nm_template + "', " +
                    "@kd_poli = '" + data.kd_poli + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: menambahkan obat pada paket obat berdasarkan kd_dokter pada paket obat non racik
        // api/Users/PostApSOTemplate_Resep
        [HttpPost("PostApSOTemplate_Resep_dt")]
        public IActionResult PostApSOTemplate_Resep_dt([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_Insert_ApSOTemplate_Resep_dt @kd_dokter = '" + data.kd_dokter + "', @kd_brg = '" + data.data[i].Kd_Brg + "', " +
                    "@jml_so = '" + data.data[i].Jml_SO + "', @aturan_pakai = '" + data.data[i].Aturan_P + "', @info_obat = '" + data.info_obat + "', @kd_poli = '" + data.kd_poli + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // Paket obat non racik


        // note: api get paket obat racik berdasarkan kode dokter
        // api/Users/GetPaketObatRacikByDok
        [HttpGet("GetPaketObatRacikByDok/{kddok}")]
        public IActionResult GetPaketObatRacikByDok(string kddok)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPaketObatRacik @Kd_Dokter = '"+ kddok +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // note: api menampilkan list obat berdasrkan paket obat yg dipilih pada paket obat non racik
        // api/Users/GetObatByPaketObatRacik
        [HttpGet("GetObatByPaketObatRacik/{kddok}/{kdpkt}")]
        public IActionResult GetObatByPaketObatRacik(string kddok, string kdpkt)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPakeObatRacikDetail @Kd_Dokter = '"+ kddok +"', @Kd_Template = '"+ kdpkt +"'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: membuat paket obat baru pada paket obat racik
        // api/Users/PostApSOTemplate_Resep_Racik
        [HttpPost("PostApSOTemplate_Resep_Racik")]
        public IActionResult PostApSOTemplate_Resep_Racik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Insert_ApSOTemplate_Resep_Racik @kd_dokter = '" + data.kd_dokter + "', @nm_template = '" + data.nm_template + "', " +
                    "@kd_poli = '" + data.kd_poli + "', @satuan = '" + data.satuan + "', @aturan_pakai = '" + data.aturan_pakai + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: menambahkan obat pada paket obat berdasarkan kd_dokter pada paket obat racik
        // api/Users/PostApSOTemplate_Resep_Racik_dt
        [HttpPost("PostApSOTemplate_Resep_Racik_dt")]
        public IActionResult PostApSOTemplate_Resep_Racik_dt([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_Insert_ApSOTemplate_Resep_Racik_dt @kd_dokter = '" + data.kd_dokter + "', @kd_template = '" + data.kd_template + "', " +
                    "@kd_poli = '" + data.kd_poli + "', @jumlah = '" + data.data[i].Jml_SO + "', @satuan = '" + data.data[i].Sat_Brg + "', @kd_barang = '" + data.data[i].Kd_Brg + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api simpan keselurahan data resep pasien
        // note: api pembuatan kode SO
        // api/Users/GetKDSO
        [HttpPost("GetKDSO")]
        public IActionResult GetKDSO([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GetKdSO @kd_poli = '" + data.kd_poli + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateApSOHeader
        [HttpPut("UpdateApSOHeader/{no_so}")]
        public IActionResult UpdateApSOHeader(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "sp_UpdateCetakStsApSOHeader @No_SO = '" + no_so.Replace("@", "/") + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/MintaSOApotikRJ
        [HttpGet("MintaSOApotikRJ/{no_so}")]
        public IActionResult MintaSOApotikRJ(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC SIRSBO.dbo.FormPermintaanSOApotikRJPolix @No_SO = '" + no_so.Replace("@", "/") + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/DeleteAllResep
        [HttpPost("DeleteAllResep")]
        public IActionResult DeleteAllResep([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteAllResep @No_So = '" + data.no_so + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/User/CekNoSOByNoReg
        [HttpGet("CekNoSOByNoReg/{no_reg}")]
        public IActionResult CekNoSOByNoReg(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekNoSOByNoReg @NoReg = '" + no_reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/PostApSOHeader
        [HttpPost("PostApSOHeader")]
        public IActionResult PostApSOHeader([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertApSOHeader @No_SO = '" + data.No_SO + "', @Kd_Poli='" + data.Kd_Poli +
                    "', @No_Reg='" + data.No_Reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/PostApSONRacik
        [HttpPost("PostApSONRacik")]
        public IActionResult PostApSONRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                // Console.WriteLine(data.data);
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_insertApSONRacik @No_SO = '" + data.No_SO + "', @Kd_Brg='" + data.data[i].Kd_Brg + "', " +
                    "@Aturan_P='" + data.data[i].Aturan_P + "', @Jml_PSO = '" + data.data[i].Jml_SO + "', @Jml_RMW = '" + data.data[i].Jml_RMW + "'";
                     // Console.WriteLine(query);
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/GetResepObatNonRacik
        [HttpGet("GetResepObatNonRacik/{no_reg}")]
        public IActionResult GetResepObatNonRacik(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetResepObatNonRacik @No_Reg='" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/DeleteApNonRacik
        [HttpPost("DeleteApNonRacik")]
        public IActionResult DeleteApNonRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_deleteObatApSONRacik @no_so = '" + data.no_so + "', @no_urut = '" + data.no_urut + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // end api baru utk app dokter

        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        // Resep Racik 

        // api/User/CekNoSOApSORacikH
        [HttpGet("CekNoSOApSORacikH/{no_so}")]
        public IActionResult CekNoSOApSORacikH(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekNoSOApSORacikH @No_SO = '" + no_so.Replace("@","/") + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // api/Users/GenerateRacikHeader
        [HttpPost("GenerateRacikHeader")]
        public IActionResult GenerateRacikHeader([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GenerateRacikHeader @no_so = '" + data.no_so + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // post ke tabel ApSoRacikH
        // api/Users/PostApSoRacikH
        [HttpPost("PostApSoRacikH")]
        public IActionResult PostApSoRacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertApSoRacikH @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                    "@no_urut = '" + data.no_urut + "', @nama_racikan = '" + data.nama_racikan + "', @jml_ns = '" + data.jml_ns + "', @satuan_racik = '" + data.satuan_racik + "', @dosis_racik = '" + data.dosis_racik + "', @info_obat_racik = '" + data.info_obat_racik + "', @catatan_racik = '" + data.catatan_racik + "', @ket_racik = '" + data.ket_racik + "', @jml_rmw = '" + data.jml_rmw + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/GetNamaRacik
        [HttpGet("GetNamaRacik/{no_so}")]
        public IActionResult GetNamaRacik(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetDataRacik @no_so='" + no_so.Replace("@","/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api untuk menampilkan data racik obat ketika update resep
        // api/Users/GetResepObatRacik
        [HttpGet("GetResepObatRacik/{no_reg}")]
        public IActionResult GetResepObatRacik(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetResepObatRacik @No_Reg='" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
        // api/Users/UpdateApSORacikH
        [HttpPut("UpdateApSORacikH")]
        public IActionResult UpdateApSORacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateRacikHeader @Jml_PRacik = '" + data.Jml_PRacik + "' , @No_SO = '" + data.No_SO + "', @Kd_Racik = '" + data.Kd_Racik + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/PostApSORacikD
        [HttpPost("PostApSORacikD")]
        public IActionResult PostApSORacikD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                // Console.WriteLine(data.data);
                for (int i = 0; i < data.dataRacik.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_InsertApSORacikD @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                        "@no_urut = '" + data.no_urut + "', @kd_brg='" + data.dataRacik[i].Kd_Brg + "', @jumlah='" + data.dataRacik[i].Jml_SO + "', @sat_brg='" + data.dataRacik[i].Sat_SO + "'";
                    //  Console.WriteLine(query);
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/GetResepObatRacik
        [HttpGet("GetResepObatRacik/{no_so}/{kd_racik}")]
        public IActionResult GetResepObatRacik(string no_so, string kd_racik)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetResepObatRacikDetail @no_so='" + no_so.Replace("@","/") + "', @kd_racik='" + kd_racik +"'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/DeleteApSORacikH
        [HttpPost("DeleteApSORacikH")]
        public IActionResult DeleteApSORacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteApSORacikH @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/DeleteApSORacikD
        [HttpPost("DeleteApSORacikD")]
        public IActionResult DeleteApSORacikD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteApSORacikD @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                    "@kd_brg = '" + data.kd_brg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateListRacik
        [HttpPut("UpdateListRacik")]
        public IActionResult UpdateListRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_EditListRacik @Jml_PSO = '" + data.Jml_PSO + "', @No_SO = '" + data.No_SO + "', " +
                        "@Kd_Racik = '" + data.Kd_Racik + "', @Kd_Brg = '" + data.Kd_Brg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        //  End Resep Racik

        // Antrian 
        // api/Users/GetTicketPasien
        [HttpGet("GetTicketPasien")]
        public IActionResult GetTicketPasien()
        {
            string query = "";
            string res = "";

            try
            {
                query = "SELECT * FROM SIRSINFO.dbo.QTicket";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/AntrianTiket
        [HttpPost("AntrianTiket")]
        public IActionResult AntrianTiket([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRS.dbo.InsQCalling @Ticket_ID = '" + data.ticket_id + "', @Counter_No = '" + data.counter_no + "', " +
                    "@Ticket_Status = '" + data.ticket_status + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Update Status Panggil 
        // api/Users/AntrianTiket
        [HttpPost("UpdStsPanggil")]
        public IActionResult UpdStsPanggil([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRSINFO.dbo.updstatusPanggil @Ticket_ID = '" + data.ticket_id + "', @Ticket_Posisi = '" + data.ticket_posisi + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // start Diagnosa Pasien

        // api/User/CekDiagByNoReg
        [HttpGet("CekDiagByNoReg/{no_reg}")]
        public IActionResult CekDiagByNoReg(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekDiagByNoReg @NoReg = '" + no_reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: api get diagnosa untuk pasien
        // api/Users/GetDiagnosaPasien
        [HttpGet("GetDiagnosaPasien/{deskterjemah}")]
        public IActionResult GetDiagnosaPasien(string deskterjemah)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetDiagnosaPasein @DESCRIPTION = '"+ deskterjemah + "', @TERJEMAHAN = '"+ deskterjemah + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = UsersController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Insert RJ Diagnosa
        // api/Users/InsertRJDiagnosa
        [HttpPost("InsertRJDiagnosa")]
        public IActionResult InsertRJDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertRJ_Diagnosa @RJ_NoReg = '" + data.RJ_NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);

                // sb = new StringBuilder();
                // string countNoreg = "SELECT COUNT(*) C FROM sirs..RJ_Diagnosa WHERE RJ_NoReg = '" + data.RJ_NoReg + "'";
                // // Console.WriteLine(countNoreg);
                // sb.AppendFormat(countNoreg);
                // sqlCommand = new SqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);
                // string cekNoreg = DbSql.GetJSONObjectString(ds.Tables[0]);
                // // Console.WriteLine(cekNoreg);

                // if (JObject.Parse(cekNoreg)["C"].ToString() == "0")
                // {
                //     query = "EXECUTE sp_InsertRJ_Diagnosa @RJ_NoReg = '" + data.RJ_NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "'";
                // }
                // else {
                //     sb = new StringBuilder();
                //     string count = "SELECT COUNT(*) D FROM sirs..icdx WHERE [DESCRIPTION] + ' | ' + [TERJEMAHAN] = '" + data.Ket_Tindakan + "'";
                //     // Console.WriteLine(count);
                //     sb.AppendFormat(count);
                //     sqlCommand = new SqlCommand(sb.ToString());
                //     ds = oDb.getDataSetFromSP(sqlCommand);
                //     string cek = DbSql.GetJSONObjectString(ds.Tables[0]);
                //     // Console.WriteLine(cek);
                //     if (JObject.Parse(cek)["D"].ToString() == "0")
                //     {
                //         query1 = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '', @Ket_ICD = '', @Ket_Tindakan = '" + data.Ket_Tindakan + "', " +
                //             "@UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                //     }
                //     else
                //     {
                //         query1 = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', " +
                //             "@Ket_Tindakan = '', @UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                //     }

                //     sb = new StringBuilder();
                //     // Console.WriteLine(query1);
                //     sb.AppendFormat(query1);
                //     sqlCommand = new SqlCommand(sb.ToString());
                //     ds = oDb.getDataSetFromSP(sqlCommand);
                //     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                // }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateRJDiagnosa
        [HttpPut("UpdateRJDiagnosa")]
        public IActionResult UpdateRJDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                string count = "SELECT COUNT(*) C FROM sirs..icdx WHERE [DESCRIPTION] + ' | ' + [TERJEMAHAN] = '" + data.Ket_Tindakan + "'";
                // Console.WriteLine(count);
                sb.AppendFormat(count);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string cek = DbSql.GetJSONObjectString(ds.Tables[0]);
                // Console.WriteLine(cek);
                if (JObject.Parse(cek)["C"].ToString() == "0")
                {
                    query = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '', @Ket_ICD = '" + data.Ket_Tindakan + "', @UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                }
                else
                {
                    query = "EXECUTE sp_UpdateRJ_Diagnosa @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', @UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                }

                sb = new StringBuilder();
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdatePemeriksaan
        [HttpPut("UpdatePemeriksaanFisik")]
        public IActionResult UpdatePemeriksaanFisik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdatePemeriksaanFisik @PemeriksaanFisik = '" + data.PemeriksaanFisik + "', @Planning = '" + data.Planning + "', @RJ_NoReg = '" + data.RJ_NoReg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateRJDiagnosa1
        [HttpPut("UpdateRJDiagnosa1")]
        public IActionResult UpdateRJDiagnosa1([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                // Console.WriteLine(data.data);
                for (int i = 0; i < data.dataTmpDiag.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_UpdateRJ_Diagnosa @UserID = '" + data.UserID + "', @RJ_NoReg = '" + data.RJ_NoReg + "', @Kd_ICD='" + data.dataTmpDiag[i].ICD_CODE + "', @Ket_ICD='" + data.dataTmpDiag[i].TERJEMAHAN + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Insert Diagnosa Detail by No Reg 
        // api/Users/InsertDiagnosaDetail
        [HttpPost("InsertDiagnosaDetail")]
        public IActionResult InsertDiagnosaDetail([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertDiagnosaDetailByNoReg @RJ_NoReg = '" + data.RJ_NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', @UserID = '" + data.UserID + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/GetDiagnosaPasienByNoReg
        [HttpGet("GetDiagnosaPasienByNoReg/{NoReg}")]
        public IActionResult GetDiagnosaPasienByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_DiagnosaPasienDetail @RJ_NoReg = '" + NoReg + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
                // Console.WriteLine(res);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // End Diagnosa Pasien

        // start ALKES
        // note: api get diagnosa untuk pasien
        // api/Users/GetAlkesObat
        [HttpGet("GetAlkesObat/{str}")]
        public IActionResult GetAlkesObat(string str)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE BrowAlkesObat @Str = '" + str + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = UsersController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: menambahkan alkes obat
        // api/Users/PostAlkesObat_RJ
        [HttpPost("PostAlkesObat_RJ")]
        public IActionResult PostAlkesObat_RJ([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE InstmpTrxRJ_AlkesObat @NoReg = '" + data.no_reg + "', @KdBarang = '" + data.data[i].Kode + "', " +
                    "@QTY = '" + data.data[i].QTY + "', @HargaSatuan = '" + data.data[i].HargaSatuan + "', @Total = '" + data.data[i].Total + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                    // Console.WriteLine(res);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // api/Users/GetAlkesObatByNoReg
        [HttpGet("GetAlkesObatByNoReg/{NoReg}")]
        public IActionResult GetAlkesObatByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetAlkesByNoReg @NoReg = '" + NoReg + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // api/Users/DeleteListAlkesObat
        [HttpPost("DeleteListAlkesObat")]
        public IActionResult DeleteListAlkesObat([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteListAlkesObatPasien @NoReg = '" + data.noreg + "', @KdBarang = '" + data.kdbrg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // end ALKES

        // start PM
        // api/Users/GetJenisPelayananPM
        [HttpGet("GetJenisPelayananPM")]
        public IActionResult GetJenisPelayananPM()
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetJenisPelayanan";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // api/Users/GeneKDPM
        [HttpPost("GeneKDPM")]
        public IActionResult GeneKDPM()
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GeneKDPM";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: inser data ke table SIRS RL_Registrasi
        // api/Users/PostLayananRegis
        [HttpPost("PostLayananRegis")]
        public IActionResult PostLayananRegis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_insertRL_Registrasi @RL_NoReg = '" + data.RL_NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "', " +
                    "@RJ_NoReg = '" + data.RJ_NoReg + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/CekKDPM
        [HttpPost("CekKDPM")]
        public IActionResult CekKDPM([FromBody] dynamic data)
        {
            string query = "";
            string cek = "";

            try
            {
                sb = new StringBuilder();
                string count = "SELECT COUNT(*) Result FROM SIRS..RL_Registrasi WHERE RL_NoReg = '" + data.Kd_PM + "'";
                sb.AppendFormat(count);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                cek = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                cek = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(cek));
        }

        // api/Users/GetPemeriksaanByKdLyn
        [HttpPost("GetPemeriksaanByKdLyn")]
        public IActionResult GetPemeriksaanByKdLyn([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPemeriksaanByKodeLyn @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tarif = '" + data.Kd_Tarif + "', " +
                    "@Kd_Dokter = '" + data.Kd_Dokter + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/GetObjPemeriksaanByKdTndk
        [HttpPost("GetObjPemeriksaanByKdTndk")]
        public IActionResult GetObjPemeriksaanByKdTndk([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetObjPemeriksaanByKdTndk @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tarif = '" + data.Kd_Tarif + "', " +
                    "@Kd_Dokter = '" + data.Kd_Dokter + "', @Kd_Tindakan = '" + data.Kd_Tindakan + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // end PM

        // Tindakan
        // api/Users/GetTindakan
        // [HttpGet("GetTindakan/{kddokter}/{kdlyn}/{str}")]
        // public IActionResult GetTindakan(string kddokter, string kdlyn, string str)
        // {
        //     string query = "";
        //     string res = "";

        //     try
        //     {
        //         // query = "EXECUTE IMAN..BrowTindakanForTrxRJ @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "', @Str = '" + str + "'";
        //         query = "EXECUTE BrowTindakanForTrxRJ @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "', @Str = '" + str + "'";
        //         // Console.WriteLine(query);
        //         sb = new StringBuilder();
        //         sb.AppendFormat(query);
        //         sqlCommand = new SqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         if (ds.Tables[0].Rows.Count != 0)
        //         {
        //             //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
        //             ds = UsersController.EscapeSpecialChar(ds);
        //         }

        //         res = DbSql.GetJSONArrayString(ds.Tables[0]);
        //     }
        //     catch
        //     {
        //         res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
        //     }

        //     return Ok(JArray.Parse(res));
        // }

        // api/Users/GetTindakanJPRS
        // [HttpGet("GetTindakanJPRS/{kddokter}/{kdlyn}")]
        // public IActionResult GetTindakanJPRS(string kddokter, string kdlyn)
        // {
        //     string query = "";
        //     string res = "";

        //     try
        //     {
        //         // query = "EXECUTE IMAN..BrowTindakanForTrxRJ @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "', @Str = '" + str + "'";
        //         query = "EXECUTE sp_GetTindakanJPRS @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "'";
        //         // Console.WriteLine(query);
        //         sb = new StringBuilder();
        //         sb.AppendFormat(query);
        //         sqlCommand = new SqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         if (ds.Tables[0].Rows.Count != 0)
        //         {
        //             //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
        //             ds = UsersController.EscapeSpecialChar(ds);
        //         }

        //         res = DbSql.GetJSONArrayString(ds.Tables[0]);
        //     }
        //     catch
        //     {
        //         res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
        //     }

        //     return Ok(JArray.Parse(res));
        // }

        // api/Users/PostTindakan
        // [HttpPost("PostTindakan")]
        // public IActionResult PostTindakan([FromBody] dynamic data)
        // {
        //     string query = "";
        //     string res = "";

        //     try
        //     {
        //         for (int i = 0; i < data.data.Count; i++)
        //         {
        //             sb = new StringBuilder();
        //             query = "EXECUTE InstmpTrxRJ_Tindakan @NoReg = '" + data.NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tindakan = '" + data.data[i].Kd_Tindakan + "', @CITO = '" + data.data[i].Cito + "', @QTY = '" + data.data[i].Qty + "', @Harga = '" + data.data[i].Harga + "', @Disc = '" + data.data[i].Disc + "', @Total = '" + data.data[i].Total + "', @HrgNormal = '" + data.data[i].Hrg_Std + "', @HrgCITO = '" + data.data[i].Hrg_CITO + "', @HrgKary = '" + data.data[i].Hrg_Kary + "'";                    
        //             Console.WriteLine(query);
        //             sb.AppendFormat(query);
        //             sqlCommand = new SqlCommand(sb.ToString());
        //             ds = oDb.getDataSetFromSP(sqlCommand);
        //             res = DbSql.GetJSONObjectString(ds.Tables[0]);
        //         }
        //     }
        //     catch
        //     {
        //         res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
        //     }

        //     return Ok(JObject.Parse(res));
        // }

        // api/Users/DeleteListTindakan
        // [HttpPost("DeleteListTindakan")]
        // public IActionResult DeleteListTindakan([FromBody] dynamic data)
        // {
        //     string query = "";
        //     string res = "";

        //     try
        //     {
        //         sb = new StringBuilder();
        //         query = "EXECUTE sp_DeleteListTindakanPasien @NoReg = '" + data.noreg + "', @Kd_Lyn = '" + data.kdlyn + "', @Kd_Tindakan = '" + data.kdtind + "'";
        //         sb.AppendFormat(query);
        //         sqlCommand = new SqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         res = DbSql.GetJSONObjectString(ds.Tables[0]);
        //     }
        //     catch
        //     {
        //         res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
        //     }

        //     return Ok(JObject.Parse(res));
        // }


        // api/Users/GetDataTindakanPasien
        // [HttpGet("GetDataTindakanPasien/{noreg}/{kdlyn}")]
        // public IActionResult GetDataTindakanPasien(string noreg, string kdlyn)
        // {
        //     string query = "";
        //     string res = "";

        //     try
        //     {
        //         // query = "EXECUTE IMAN..BrowTindakanForTrxRJ @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "', @Str = '" + str + "'";
        //         query = "EXECUTE sp_GetDataTindakan @NoReg = '" + noreg + "', @Kd_Lyn = '" + kdlyn + "'";
        //         // Console.WriteLine(query);
        //         sb = new StringBuilder();
        //         sb.AppendFormat(query);
        //         sqlCommand = new SqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         res = DbSql.GetJSONArrayString(ds.Tables[0]);
        //     }
        //     catch
        //     {
        //         res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
        //     }

        //     return Ok(JArray.Parse(res));
        // }

        // Tindakan

        // Edit Resep Obat Non Racik
        // api/Users/EditResepObatNonRacik
        [HttpGet("EditResepObatNonRacik/{noso}/{kdbrg}")]
        public IActionResult EditResepObatNonRacik(string noso, string kdbrg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetResepObatNonRacikEdit @No_SO = '" + noso.Replace("@", "/") + "', @Kd_Brg = '" + kdbrg + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/GetSkrinningPasien
        [HttpGet("GetSkrinningPasien/{noreg}")]
        public IActionResult GetSkrinningPasien(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetSkrinningPasienRJ @NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateSkrinningPasien
        [HttpPut("UpdateSkrinningPasien")]
        public IActionResult UpdateSkrinningPasien([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateSkrinningPasien @KeluhanUtama = '" + data.KeluhanUtama + "', @NoReg = '" + data.NoReg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }


        // Start Riwayat Diagnosa
        // api/Users/GetAllDiagnosaPasien
        [HttpGet("GetAllDiagnosaPasien/{kdrm}")]
        public IActionResult GetAllDiagnosaPasien(string kdrm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetAllRiwayatDiagnosaPasien @Kd_RM='" + kdrm + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/GetDetailDiagnosaPasien
        [HttpGet("GetDetailDiagnosaPasien/{kdrm}/{noreg}")]
        public IActionResult GetDetailDiagnosaPasien(string kdrm, string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetDetailRiwayatDiagnosaPasien @Kd_RM='" + kdrm + "', @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Start Riwayat Resep Non Racik
        // api/Users/GetAllRiwayatResepNRacik
        [HttpGet("GetAllRiwayatResepNRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepNRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetAllResepObatNRacik @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Start Riwayat Resep Racik
        // api/Users/GetAllRiwayatResepRacik
        [HttpGet("GetAllRiwayatResepRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetAllResepObatRacik @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/StatusAwalPraktekDokter
        [HttpPost("StatusAwalPraktekDokter")]
        public IActionResult StatusAwalPraktekDokter([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_StatusAwalPrakterDokter @Kd_Poli = '" + data.Kd_Poli + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Status Praktek Mulai
        // api/Users/UpdateStatusPraktekMulai
        [HttpPut("UpdateStatusPraktekMulai")]
        public IActionResult UpdateStatusPraktekMulai([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateStatusPraktekMulai @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Dokter = '" + data.Kd_Dokter + "', @Jam_Praktek = '" + data.Jam_Praktek + "', @Kd_Poli = '" + data.Kd_Poli + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Status Praktek Selesai
        // api/Users/UpdateStatusPraktekMulai
        [HttpPut("UpdateStatusPraktekSelesai")]
        public IActionResult UpdateStatusPraktekSelesai([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateStatusPraktekSelesai @Kd_Poli = '" + data.Kd_Poli + "',  @Tgl_Masuk = '" + data.Tgl_Masuk + "',  " +
                    "@Kd_Lyn = '" + data.Kd_Lyn + "',  @Kd_Dokter = '" + data.Kd_Dokter + "',  @Jam_Praktek = '" + data.Jam_Praktek + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateALergiApSOHeader
        [HttpPut("UpdateALergiApSOHeader/{no_reg}")]
        public IActionResult UpdateALergiApSOHeader([FromBody] dynamic data, string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateAlergiApSOHeader @Alergi = '" + data.alergi + "',  @NoReg = '" + no_reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/UpdateALergidbPasienRS
        [HttpPut("UpdateALergidbPasienRS/{kd_rm}")]
        public IActionResult UpdateALergidbPasienRS([FromBody] dynamic data, string kd_rm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_UpdateAlergidbPasienRS @Alergi = '" + data.alergi + "',  @Kd_RM = '" + kd_rm + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Users/GetObjKdKelas
        [HttpGet("GetObjKdKelas/{noreg}")]
        public IActionResult GetObjKdKelas(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC IMAN..SeekRegistrasiForPM @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Users/GetTrxPasienRM
        [HttpPost("GetTrxPasienRM")]
        public IActionResult GetTrxPasienRM([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GetTrxByRM @Kd_RM = '" + data.Kd_RM + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
        
        // api/Users/GetHasilLabByTrx
        [HttpPost("GetHasilLabByTrx")]
        public IActionResult GetHasilLabByTrx([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..RptHasilLaboratorium @No_Trx = '" + data.No_Trx + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
    }
}