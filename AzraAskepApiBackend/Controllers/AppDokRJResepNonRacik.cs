using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/AppDokterRJResepNonRacik")]
    public class AppDokterRJResepNonRacik : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public AppDokterRJResepNonRacik(ASKEPContext context)
        {
            _context = context;
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // api/AppDokterRJResepNonRacik/ObatResep
        [HttpGet("ObatResep/{kddok}")]
        public IActionResult ObatResep(string kddok)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_DataObat @kddok='" + kddok + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = UsersController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: api pembuatan kode SO
        // api/AppDokterRJResepNonRacik/GetKDSO
        [HttpPost("GetKDSO")]
        public IActionResult GetKDSO([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GetKdSO @kd_poli = '" + data.kd_poli + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/CekNoSO
        [HttpGet("CekNoSO/{no_so}")]
        public IActionResult CekNoSO(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekNoSO @No_SO = '" + no_so.Replace("@", "/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/CekNoSOByNoReg
        [HttpGet("CekNoSOByNoReg/{no_reg}")]
        public IActionResult CekNoSOByNoReg(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_CekNoSOByNoReg @NoReg = '" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/GetResepObatNonRacik
        [HttpGet("GetResepObatNonRacik/{no_reg}")]
        public IActionResult GetResepObatNonRacik(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC sp_GetResepObatNonRacik @No_Reg='" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/PostApSOHeader
        [HttpPost("PostApSOHeader")]
        public IActionResult PostApSOHeader([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_InsertApSOHeader @No_SO = '" + data.No_SO + "', @Kd_Poli='" + data.Kd_Poli +
                    "', @No_Reg='" + data.No_Reg + "'";
                Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/UpdateApSOHeader
        [HttpPut("UpdateApSOHeader/{no_so}")]
        public IActionResult UpdateApSOHeader(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "sp_UpdateCetakStsApSOHeader @No_SO = '" + no_so.Replace("@", "/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/PostApSONRacik
        [HttpPost("PostApSONRacik")]
        public IActionResult PostApSONRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_insertApSONRacik @No_SO = '" + data.No_SO + "', " + 
                    "@Kd_Brg='" + data.data[i].Kd_Brg + "', @Aturan_P='" + data.data[i].Aturan_P + "', " + 
                    "@Jml_PSO = '" + data.data[i].Jml_SO + "', @Jml_RMW = '" + data.data[i].Jml_RMW + "'";
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/DeleteAllResep
        [HttpPost("DeleteAllResep")]
        public IActionResult DeleteAllResep([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteAllResep @No_So = '" + data.no_so + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterRJResepNonRacik/DeleteApNonRacik
        [HttpPost("DeleteApNonRacik")]
        public IActionResult DeleteApNonRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_deleteObatApSONRacik @no_so = '" + data.no_so + "', @no_urut = '" + data.no_urut + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Paket obat non racik
        // note: api get paket obat non racik berdasarkan kode dokter
        // api/AppDokterRJResepNonRacik/GetPaketObatByDok
        [HttpGet("GetPaketObatByDok/{kddok}")]
        public IActionResult GetPaketObatByDok(string kddok)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPaketObatNonRacik @Kd_Dokter = '"+ kddok +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // note: api menampilkan list obat berdasrkan paket obat yg dipilih pada paket obat non racik
        // api/AppDokterRJResepNonRacik/GetObatByPaketObat
        [HttpGet("GetObatByPaketObat/{kddok}/{kdpkt}")]
        public IActionResult GetObatByPaketObat(string kddok, string kdpkt)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetPaketObatNonRacikDetail @Kd_Dokter = '"+ kddok +"',  @Kd_Template = '"+ kdpkt +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: membuat paket obat baru pada paket obat non racik
        // api/AppDokterRJResepNonRacik/PostApSOTemplate_Resep
        [HttpPost("PostApSOTemplate_Resep")]
        public IActionResult PostApSOTemplate_Resep([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Insert_ApSOTemplate_Resep @kd_dokter = '" + data.kd_dokter + "', @nm_template = '" + data.nm_template + "', " +
                    "@kd_poli = '" + data.kd_poli + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: menambahkan obat pada paket obat berdasarkan kd_dokter pada paket obat non racik
        // api/AppDokterRJResepNonRacik/PostApSOTemplate_Resep
        [HttpPost("PostApSOTemplate_Resep_dt")]
        public IActionResult PostApSOTemplate_Resep_dt([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_Insert_ApSOTemplate_Resep_dt @kd_dokter = '" + data.kd_dokter + "', @kd_brg = '" + data.data[i].Kd_Brg + "', " +
                    "@jml_so = '" + data.data[i].Jml_SO + "', @aturan_pakai = '" + data.data[i].Aturan_P + "', @info_obat = '" + data.info_obat + "', @kd_poli = '" + data.kd_poli + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
        // Paket obat non racik
    }
}