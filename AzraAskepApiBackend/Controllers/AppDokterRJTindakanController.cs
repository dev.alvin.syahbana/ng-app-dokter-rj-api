using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/Tindakan")]
    public class TindakanController : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public TindakanController(ASKEPContext context)
        {
            _context = context;
        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // api/Tindakan/GetTindakanJPRS
        [HttpGet("GetTindakanJPRS/{kddokter}/{kdlyn}")]
        public IActionResult GetTindakanJPRS(string kddokter, string kdlyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetTindakanJPRS @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = TindakanController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Tindakan/GetTindakan
        [HttpGet("GetTindakan/{kddokter}/{kdlyn}/{str}")]
        public IActionResult GetTindakan(string kddokter, string kdlyn, string str)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE BrowTindakanForTrxRJ @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "', @Str = '" + str + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = UsersController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Tindakan/PostTindakan
        [HttpPost("PostTindakan")]
        public IActionResult PostTindakan([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE InstmpTrxRJ_Tindakan @NoReg = '" + data.NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tindakan = '" + data.data[i].Kd_Tindakan + "', @CITO = '" + data.data[i].Cito + "', @QTY = '" + data.data[i].Qty + "', @Harga = '" + data.data[i].Harga + "', @Disc = '" + data.data[i].Disc + "', @Total = '" + data.data[i].Total + "', @HrgNormal = '" + data.data[i].Hrg_Std + "', @HrgCITO = '" + data.data[i].Hrg_CITO + "', @HrgKary = '" + data.data[i].Hrg_Kary + "'";                    
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Tindakan/DeleteListTindakan
        [HttpPost("DeleteListTindakan")]
        public IActionResult DeleteListTindakan([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteListTindakanPasien @NoReg = '" + data.noreg + "', @Kd_Lyn = '" + data.kdlyn + "', @Kd_Tindakan = '" + data.kdtind + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }


        // api/Tindakan/GetDataTindakanPasien
        [HttpGet("GetDataTindakanPasien/{noreg}/{kdlyn}")]
        public IActionResult GetDataTindakanPasien(string noreg, string kdlyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetDataTindakan @NoReg = '" + noreg + "', @Kd_Lyn = '" + kdlyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
    }	
}